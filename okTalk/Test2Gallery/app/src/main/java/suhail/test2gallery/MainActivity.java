package suhail.test2gallery;

import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;

public class MainActivity extends BaseRecycleViewActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setLayoutManager(new GridLayoutManager(this, 3));
        }else{
            setLayoutManager(new GridLayoutManager(this, 5));
        }
        setAdapter(new GalleryAdapter(this));

        loadGalleryImages();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return (new CursorLoader(this,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, null, null,
                MediaStore.Images.Media.TITLE));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        ((GalleryAdapter) getAdapter()).setVideos(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        ((GalleryAdapter) getAdapter()).setVideos(null);
    }


    private void loadGalleryImages() {
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ((GridLayoutManager) getLayoutManager()).setSpanCount(5);
            getRecyclerView().getAdapter().notifyDataSetChanged();
        } else {
            ((GridLayoutManager) getLayoutManager()).setSpanCount(3);
            getRecyclerView().getAdapter().notifyDataSetChanged();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getRecyclerView().clearOnScrollListeners();
    }
}
