package suhail.test2gallery;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by Suhail on 16/09/16.
 */
public class GalleryHolder extends RecyclerView.ViewHolder {

    private DynamicHeightImageView thumbnail = null;
    private Uri imageUri = null;

    GalleryHolder(View row) {
        super(row);

        thumbnail = (DynamicHeightImageView) row.findViewById(R.id.thumbnail);
        thumbnail.setHeightRatio(1.0);

    }

    void bindModel(Cursor row) {

        imageUri = ContentUris.withAppendedId(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                row.getInt(row.getColumnIndex(MediaStore.Images.Media._ID)));

        Picasso.with(thumbnail.getContext())
                .load(imageUri.toString())
                .fit()
                .placeholder(R.mipmap.ic_launcher)
                .into(thumbnail);

    }

}
