package suhail.test2gallery;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by Suhail on 16/09/16.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryHolder> {

    private Cursor cursor = null;
    private Context mContext;

    public GalleryAdapter(Context context) {
        mContext = context;
    }

    @Override
    public GalleryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return (new GalleryHolder(((Activity) mContext).getLayoutInflater()
                .inflate(R.layout.row, parent, false)));
    }

    void setVideos(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(GalleryHolder holder, int position) {
        cursor.moveToPosition(position);
        holder.bindModel(cursor);
    }

    @Override
    public int getItemCount() {
        if (cursor == null) {
            return (0);
        }

        return (cursor.getCount());
    }
}
