package suhail.test2gallery;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Suhail on 16/09/16.
 */
public class BaseRecycleViewActivity extends AppCompatActivity {

    private RecyclerView recyclerView = null;

    public void setAdapter(RecyclerView.Adapter adapter) {
        getRecyclerView().setAdapter(adapter);
    }

    public RecyclerView.Adapter getAdapter() {
        return (getRecyclerView().getAdapter());
    }

    public void setLayoutManager(RecyclerView.LayoutManager mgr) {
        getRecyclerView().setLayoutManager(mgr);
    }

    public RecyclerView.LayoutManager getLayoutManager(){
        return getRecyclerView().getLayoutManager();
    }

    public RecyclerView getRecyclerView() {
        if (recyclerView == null) {
            recyclerView = new RecyclerView(this);
            setContentView(recyclerView);
        }

        return (recyclerView);
    }
}
