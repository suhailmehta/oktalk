package suhail.test1fragments.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import suhail.test1fragments.R;

/**
 * Created by Suhail on 15/09/16.
 */
public class TextViewFragment extends Fragment {

    private TextView demoTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_text_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        demoTextView = (TextView) view.findViewById(R.id.demo_text_view);

    }

    public String getDemoText() {
        return demoTextView.getText().toString();
    }

    public void setDemoText(String demoText) {
        demoTextView.setText(demoText);
    }
}
