package suhail.test1fragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import suhail.test1fragments.fragments.TextViewFragment;
import suhail.test1fragments.interfaces.OnTextChangeListener;

/**
 * Created by suhail on 15/09/16.
 */
public class MainActivity extends AppCompatActivity implements OnTextChangeListener {

    private TextViewFragment textViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewFragment = (TextViewFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_text_view);

    }

    @Override
    public void onTextChange(String text) {
        textViewFragment.setDemoText(text);
    }
}
