package suhail.test1fragments.interfaces;

/**
 * Created by Suhail on 15/09/16.
 */
public interface OnTextChangeListener {

    void onTextChange(String text);

}
